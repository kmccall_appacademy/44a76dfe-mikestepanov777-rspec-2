def measure(run = 0)
  start = Time.now
  if run == 0
    yield
  else
    run.times {yield}
  end
  run > 0 ? (Time.now - start) / run : Time.now - start
end

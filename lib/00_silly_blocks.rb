def reverser
  yield.split(" ").map(&:reverse).join(" ")
end

def adder(add = 1)
  yield + add
end

def repeater(times = 1)
  times.times {yield}
end
